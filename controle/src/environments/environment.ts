// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: 'AIzaSyBYfjBm__78hr06AgR0kkSkXQb8M5AeoUc',
    authDomain: 'controle-button.firebaseapp.com',
    projectId: 'controle-button',
    storageBucket: 'controle-button.appspot.com',
    messagingSenderId: '304662982899',
    appId: '1:304662982899:web:78e9dcae4e350518c70786'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
